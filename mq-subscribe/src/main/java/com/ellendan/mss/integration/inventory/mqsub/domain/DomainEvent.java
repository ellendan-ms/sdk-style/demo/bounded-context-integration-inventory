package com.ellendan.mss.integration.inventory.mqsub.domain;

import java.time.LocalDateTime;

public interface DomainEvent<ID> {
    ID getAggregationId();
    LocalDateTime getOccurrenceOn();
}
