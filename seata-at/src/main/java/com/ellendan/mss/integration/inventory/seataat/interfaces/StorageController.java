package com.ellendan.mss.integration.inventory.seataat.interfaces;

import com.ellendan.mss.integration.inventory.common.domain.Storage;
import com.ellendan.mss.integration.inventory.seataat.service.StorageService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RequiredArgsConstructor
@RestController
@RequestMapping("/storages")
public class StorageController {
    private final StorageService productStorageService;

    @GetMapping("/{productId}")
    Storage fetchLatestCount(@PathVariable String productId) {
        return productStorageService.fetchLatest(productId);
    }

    @PutMapping("/{productId}")
    Storage minusStorage(@RequestBody MinusStorageRequest minusStorageRequest){
        return productStorageService.minusStorageByNewOrder(minusStorageRequest.getProductId(),
                minusStorageRequest.getQuantity());
    }

}
