package com.ellendan.mss.integration.inventory;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@EnableFeignClients
@EnableJpaAuditing
@EntityScan(basePackages = {"com.ellendan.mss.integration.inventory.common", "com.ellendan.mss.integration.inventory.rpcsub"})
@EnableJpaRepositories(basePackages = {"com.ellendan.mss.integration.inventory.common", "com.ellendan.mss.integration.inventory.rpcsub"})
@SpringBootApplication
public class RpcSubscriptionApp {

    public static void main(String[] args) {
        SpringApplication.run(RpcSubscriptionApp.class, args);
    }
}
