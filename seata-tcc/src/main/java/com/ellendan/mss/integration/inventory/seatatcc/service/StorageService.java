package com.ellendan.mss.integration.inventory.seatatcc.service;

import com.ellendan.mss.integration.inventory.common.domain.Storage;
import com.ellendan.mss.integration.inventory.common.infrastructure.jpa.StorageRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class StorageService {
    private final StorageRepository productStorageRepository;
    private final MinusStorageTccService minusStorageTccService;

    public Storage minusStorageByNewOrder(String productId, Integer quantity) {
        return minusStorageTccService.prepare(null, productId, quantity);
    }

    public Storage fetchLatest(String productId) {
        return productStorageRepository.findByProductId(productId)
                .orElseGet(null);
    }
}
