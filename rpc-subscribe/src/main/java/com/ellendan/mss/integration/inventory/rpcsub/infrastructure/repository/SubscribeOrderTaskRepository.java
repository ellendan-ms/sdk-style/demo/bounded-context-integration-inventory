package com.ellendan.mss.integration.inventory.rpcsub.infrastructure.repository;

import com.ellendan.mss.integration.inventory.rpcsub.domain.SubscribeOrderTask;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface SubscribeOrderTaskRepository extends JpaRepository<SubscribeOrderTask, String> {
    Optional<SubscribeOrderTask> findByProductId(String productId);
}
