package com.ellendan.mss.integration.inventory.mqsub.infrastructure.jpa;

import com.ellendan.mss.integration.inventory.mqsub.domain.ConsumedMessage;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ConsumedMessageRepository extends JpaRepository<ConsumedMessage, Long> {
}
