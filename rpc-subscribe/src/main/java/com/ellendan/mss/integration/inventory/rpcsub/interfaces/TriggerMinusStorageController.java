package com.ellendan.mss.integration.inventory.rpcsub.interfaces;

import com.ellendan.mss.integration.inventory.rpcsub.service.MinusStorageApplicationService;
import com.ellendan.mss.integration.inventory.common.domain.Storage;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RequiredArgsConstructor
@RestController
@RequestMapping("/minus-storages")
public class TriggerMinusStorageController {
    private final MinusStorageApplicationService minusStorageApplicationService;

    @GetMapping
    public Storage subscribeNewOrders(@RequestParam String productId) {
        return minusStorageApplicationService.minusStorageByOrders(productId);
    }
}
