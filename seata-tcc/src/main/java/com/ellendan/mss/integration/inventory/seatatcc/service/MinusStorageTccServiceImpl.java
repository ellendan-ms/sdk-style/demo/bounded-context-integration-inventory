package com.ellendan.mss.integration.inventory.seatatcc.service;

import com.ellendan.mss.integration.inventory.common.domain.Storage;
import com.ellendan.mss.integration.inventory.common.infrastructure.jpa.StorageRepository;
import io.seata.rm.tcc.api.BusinessActionContext;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


@Slf4j
@Service
@RequiredArgsConstructor
public class MinusStorageTccServiceImpl implements MinusStorageTccService {
    private final StorageRepository storageRepository;

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Storage prepare(BusinessActionContext actionContext,
                           String productId, Integer quantity) {
        log.info("Minus storage prepare, xid {}", actionContext.getXid());
        if (System.currentTimeMillis() % 3 == 0) {
            log.error("Random rollback");
            throw new RuntimeException("Random rollback order created");
        }

        Storage productStorage = storageRepository.findByProductId(productId)
                .orElseThrow(() -> {
                    log.error("product storage data not exists, product id [{}]", productId);
                    return new RuntimeException("product storage data not exists");
                });
        productStorage = productStorage.minusStorage(quantity);
        return storageRepository.save(productStorage);
    }

    @Override
    public void commit(BusinessActionContext actionContext) {
        log.info("minus storage succeed, xid {}", actionContext.getXid());
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void rollback(BusinessActionContext actionContext) {
        String productId = actionContext.getActionContext("productId", String.class);
        Integer quantity = actionContext.getActionContext("quantity", Integer.class);
        log.info("minus storage rollback, xid {}, productId {}, quantity {}",
                actionContext.getXid(), productId, quantity);

        Storage productStorage = storageRepository.findByProductId(productId)
                .orElseThrow(() -> {
                    log.error("product storage data not exists, product id [{}]", productId);
                    return new RuntimeException("product storage data not exists");
                });
        productStorage = productStorage.minusStorage(quantity * -1);
        storageRepository.save(productStorage);
    }
}
