package com.ellendan.mss.integration.inventory.seataat.interfaces;

import lombok.Data;

@Data
public class MinusStorageRequest {
    private String productId;
    private Integer quantity;
}
