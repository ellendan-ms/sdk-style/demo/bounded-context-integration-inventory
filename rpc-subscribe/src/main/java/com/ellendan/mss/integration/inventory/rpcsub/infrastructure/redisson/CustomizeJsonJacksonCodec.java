package com.ellendan.mss.integration.inventory.rpcsub.infrastructure.redisson;

import org.redisson.codec.JsonJacksonCodec;

public class CustomizeJsonJacksonCodec extends JsonJacksonCodec {
    public CustomizeJsonJacksonCodec() {
        super();
        this.getObjectMapper().findAndRegisterModules();
    }
}
