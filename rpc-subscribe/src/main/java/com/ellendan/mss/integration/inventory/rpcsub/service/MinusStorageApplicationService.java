package com.ellendan.mss.integration.inventory.rpcsub.service;

import com.ellendan.mss.integration.inventory.common.infrastructure.jpa.StorageRepository;
import com.ellendan.mss.integration.inventory.rpcsub.domain.StorageOrder;
import com.ellendan.mss.integration.inventory.rpcsub.domain.SubscribeOrderTask;
import com.ellendan.mss.integration.inventory.rpcsub.infrastructure.client.OrderFeignClient;
import com.ellendan.mss.integration.inventory.rpcsub.infrastructure.client.SubscribeOrderRequest;
import com.ellendan.mss.integration.inventory.common.domain.Storage;
import com.ellendan.mss.integration.inventory.rpcsub.infrastructure.repository.StorageOrderRepository;
import com.ellendan.mss.integration.inventory.rpcsub.infrastructure.repository.SubscribeOrderTaskRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Optional;

@Slf4j
@Service
@RequiredArgsConstructor
public class MinusStorageApplicationService {
    private final StorageOrderRepository storageOrderRepository;
    private final StorageRepository storageRepository;
    private final SubscribeOrderTaskRepository subscribeOrderTaskRepository;
    private final OrderFeignClient orderFeignClient;

    @Transactional(rollbackFor = Throwable.class)
    public Storage minusStorageByOrders(String productId) {
        Optional<SubscribeOrderTask> subscribeOrderTaskOptional = subscribeOrderTaskRepository.findByProductId(productId);
        SubscribeOrderTask subscribeOrderTask = null;

        if(subscribeOrderTaskOptional.isEmpty()) {
            subscribeOrderTask = SubscribeOrderTask.builder()
                    .productId(productId)
                    .timestamp(Instant.now().truncatedTo(ChronoUnit.DAYS).toEpochMilli())
                    .currentPage(1)
                    .pageSize(100)
                    .build();
        } else {
            subscribeOrderTask = subscribeOrderTaskOptional.get();
        }

        List<StorageOrder> orderList = orderFeignClient.subscribeNewOrders(SubscribeOrderRequest
                .fromEntity(subscribeOrderTask));
        storageOrderRepository.saveAll(orderList);

        int needMinusCount = orderList.stream()
                .mapToInt(StorageOrder::getQuantity)
                .sum();
        Optional<Storage> productStorage = storageRepository.findByProductId(productId);
        Storage existsStorage = productStorage.orElseThrow(()-> {
            log.error("product storage data not exists, product id [{}]", productId);
            return new RuntimeException("product storage data not exists");
        });

        Integer oldQuantity = existsStorage.getQuantity();
        existsStorage.setQuantity(oldQuantity - needMinusCount);
        existsStorage = storageRepository.save(existsStorage);

        subscribeOrderTask.setCurrentPage(subscribeOrderTask.getCurrentPage() + 1);
        subscribeOrderTaskRepository.save(subscribeOrderTask);
        return existsStorage;
    }
}
