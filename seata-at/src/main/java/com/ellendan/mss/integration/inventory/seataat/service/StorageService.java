package com.ellendan.mss.integration.inventory.seataat.service;

import com.ellendan.mss.integration.inventory.common.domain.Storage;
import com.ellendan.mss.integration.inventory.common.infrastructure.jpa.StorageRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Slf4j
@Service
@RequiredArgsConstructor
public class StorageService {
    private final StorageRepository productStorageRepository;

    @Transactional(rollbackFor = Exception.class)
    public Storage minusStorageByNewOrder(String productId, Integer quantity) {
        Storage productStorage = productStorageRepository.findByProductId(productId)
                .orElseThrow(() -> {
                    log.error("product storage data not exists, product id [{}]", productId);
                    return new RuntimeException("product storage data not exists");
                });
        productStorage = productStorage.minusStorage(quantity);
        productStorageRepository.save(productStorage);
        return productStorage;
    }

    public Storage fetchLatest(String productId) {
        return productStorageRepository.findByProductId(productId)
                .orElseGet(null);
    }
}
