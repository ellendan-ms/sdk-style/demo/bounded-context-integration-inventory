package com.ellendan.mss.integration.inventory.rpcsub.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Data
@Builder
@Entity
@Table(name = "subscribe_order_tasks")
@NoArgsConstructor
@AllArgsConstructor
public class SubscribeOrderTask {
    @Id
    private String productId;
    private Long timestamp;
    private Integer currentPage;
    private Integer pageSize;
}
