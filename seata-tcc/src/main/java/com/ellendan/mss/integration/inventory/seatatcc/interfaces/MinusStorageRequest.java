package com.ellendan.mss.integration.inventory.seatatcc.interfaces;

import lombok.Data;

@Data
public class MinusStorageRequest {
//    private String orderId;
    private final String productId;
    private final Integer quantity;
}
