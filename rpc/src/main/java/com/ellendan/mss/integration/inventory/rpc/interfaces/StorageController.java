package com.ellendan.mss.integration.inventory.rpc.interfaces;

import com.ellendan.mss.integration.inventory.common.domain.Storage;
import com.ellendan.mss.integration.inventory.common.infrastructure.jpa.StorageRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Random;

@RequiredArgsConstructor
@RestController
@RequestMapping("/storages")
public class StorageController {
    private final StorageRepository storageRepository;
    private Random random = new Random(123);

    @GetMapping("/{productId}")
    Storage fetchLatestCount(@PathVariable String productId) {
        if(random.nextInt() % 3 == 0) {
            throw new RuntimeException("random exception");
        } else {
            return storageRepository.findByProductId(productId)
                    .orElseGet(null);
        }
    }
}
