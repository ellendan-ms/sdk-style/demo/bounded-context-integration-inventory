package com.ellendan.mss.integration.inventory.redisson.interfaces;

import com.ellendan.mss.integration.inventory.common.domain.Storage;
import com.ellendan.mss.integration.inventory.common.infrastructure.jpa.StorageRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RequiredArgsConstructor
@RestController
@RequestMapping("/storages")
public class StorageController {
    private final StorageRepository storageRepository;

    @GetMapping("/{productId}")
    Storage fetchLatestCount(@PathVariable String productId) {
        return storageRepository.findByProductId(productId)
                .orElseGet(null);
    }
}
