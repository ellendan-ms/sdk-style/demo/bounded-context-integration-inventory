package com.ellendan.mss.integration.inventory.mqsub.domain;

import lombok.Builder;
import lombok.Data;

import java.time.LocalDateTime;

@Data
@Builder
public class OrderCreatedEvent implements DomainEvent<String> {
    private String aggregationId;
//    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss'Z'")
    private LocalDateTime occurrenceOn;
    private String productId;
    private Integer quantity;
}
