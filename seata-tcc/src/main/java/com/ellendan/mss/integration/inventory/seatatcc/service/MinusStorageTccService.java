package com.ellendan.mss.integration.inventory.seatatcc.service;

import com.ellendan.mss.integration.inventory.common.domain.Storage;
import io.seata.rm.tcc.api.BusinessActionContext;
import io.seata.rm.tcc.api.BusinessActionContextParameter;
import io.seata.rm.tcc.api.LocalTCC;
import io.seata.rm.tcc.api.TwoPhaseBusinessAction;

@LocalTCC
public interface MinusStorageTccService {
    @TwoPhaseBusinessAction(name = "orderCreateMinusStorageBean", commitMethod = "commit",
            rollbackMethod = "rollback", useTCCFence = true)
    Storage prepare(BusinessActionContext actionContext,
                    @BusinessActionContextParameter("productId") String productId,
                    @BusinessActionContextParameter("quantity") Integer quantity);

    void commit(BusinessActionContext actionContext);
    void rollback(BusinessActionContext actionContext);
}
