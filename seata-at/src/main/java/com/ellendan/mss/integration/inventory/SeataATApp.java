package com.ellendan.mss.integration.inventory;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@EnableFeignClients
@EnableJpaAuditing
@EntityScan
@EnableJpaRepositories
@SpringBootApplication
public class SeataATApp {
    public static void main(String[] args) {
        SpringApplication.run(SeataATApp.class, args);
    }
}
