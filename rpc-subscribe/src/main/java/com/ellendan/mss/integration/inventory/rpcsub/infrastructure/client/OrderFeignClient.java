package com.ellendan.mss.integration.inventory.rpcsub.infrastructure.client;

import com.ellendan.mss.integration.inventory.rpcsub.domain.StorageOrder;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

@FeignClient(name = "orderClient", url = "127.0.0.1:8080")
public interface OrderFeignClient {

    @PostMapping("/subscribe-orders")
    List<StorageOrder> subscribeNewOrders(@RequestBody SubscribeOrderRequest subscribeOrderRequest);
}
