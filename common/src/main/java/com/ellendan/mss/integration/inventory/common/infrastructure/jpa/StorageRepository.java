package com.ellendan.mss.integration.inventory.common.infrastructure.jpa;


import com.ellendan.mss.integration.inventory.common.domain.Storage;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface StorageRepository extends JpaRepository<Storage, String> {
    Optional<Storage> findByProductId(String productId);
}
