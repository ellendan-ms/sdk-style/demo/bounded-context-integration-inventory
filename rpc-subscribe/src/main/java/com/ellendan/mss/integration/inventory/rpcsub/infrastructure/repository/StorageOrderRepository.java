package com.ellendan.mss.integration.inventory.rpcsub.infrastructure.repository;

import com.ellendan.mss.integration.inventory.rpcsub.domain.StorageOrder;
import org.springframework.data.jpa.repository.JpaRepository;

public interface StorageOrderRepository extends JpaRepository<StorageOrder, String> {
}
