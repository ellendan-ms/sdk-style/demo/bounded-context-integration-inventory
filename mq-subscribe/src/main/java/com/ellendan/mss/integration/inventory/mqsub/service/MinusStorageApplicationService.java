package com.ellendan.mss.integration.inventory.mqsub.service;

import com.ellendan.mss.integration.inventory.common.domain.Storage;
import com.ellendan.mss.integration.inventory.common.infrastructure.jpa.StorageRepository;
import com.ellendan.mss.integration.inventory.mqsub.domain.ConsumedMessage;
import com.ellendan.mss.integration.inventory.mqsub.domain.DomainEvent;
import com.ellendan.mss.integration.inventory.mqsub.domain.OrderCreatedEvent;
import com.ellendan.mss.integration.inventory.mqsub.infrastructure.jpa.ConsumedMessageRepository;
import com.ellendan.mss.integration.inventory.mqsub.interfaces.EventMessage;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.retry.RetryException;
import org.springframework.retry.annotation.Retryable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Slf4j
@RequiredArgsConstructor
@Service
public class MinusStorageApplicationService {
    private final StorageRepository productStorageRepository;
    private final ConsumedMessageRepository consumedMessageRepository;
    private final ConsumedMessageFactory consumedMessageFactory;

    @Retryable(include = RetryException.class, exclude = IllegalArgumentException.class)
    @Transactional(rollbackFor = Throwable.class)
    public void minusStorageByOrders(EventMessage eventMessage) {
        if (consumedMessageRepository.findById(eventMessage.getId()).isPresent()) {
            log.info("Message duplicated sent");
            return;
        }
        Pair<ConsumedMessage, DomainEvent> paired = consumedMessageFactory
                .createConsumedMessage(eventMessage);

        OrderCreatedEvent orderCreatedEvent = (OrderCreatedEvent) paired.getRight();
        Optional<Storage> productStorage = productStorageRepository
                .findByProductId(orderCreatedEvent.getProductId());
        Storage existsProductStorage = productStorage.orElseThrow(() -> {
            log.error("product storage data not exists, product id [{}]", orderCreatedEvent.getProductId());
            return new RuntimeException("product storage data not exists");
        });

        Integer oldQuantity = existsProductStorage.getQuantity();
        existsProductStorage.setQuantity(oldQuantity - orderCreatedEvent.getQuantity());
        productStorageRepository.save(existsProductStorage);
        consumedMessageRepository.save(paired.getLeft());
    }
}
